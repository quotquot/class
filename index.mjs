/*
 * Copyright 2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import espr from "espr";
import logging from "logging";

import { AttrRenderer, BaseHandler } from "@quotquot/element";

const logger = logging.getLogger("@quotquot/class");

class ClassHandler extends BaseHandler {

    static #ATTR_NAME = "data-class";

    static getAttributeNodes(el) {
        const attr = el.getAttributeNode(ClassHandler.#ATTR_NAME);
        return attr ? [attr] : null;
    }

    #el;
    #classes;

    constructor(renderer, el, parent, attrs) {
        super(parent);
        this.#el = el;
        this.#classes = new Map();

        const expression = espr.getFromAttr(attrs[0]);
        if (expression.type !== "ObjectExpression")
            throw new Error("invalid expression");
        for (const property of expression.properties) {
            if (property.type !== "Property")
                throw new Error("invalid expression");
            let attrName;
            switch (property.key.type) {
                case "Identifier": attrName = "name"; break;
                case "Literal": attrName = "value"; break;
                default: throw new Error("invalid expression");
            }
            this.#classes.set(property.key[attrName], {
                ast: property.value,
                vars: espr.variables(property.value)
            });
        }
    }

    async render(manager, newState, context) {
        this.setContext(context);
        const values = this.getValues(newState);
        for (const [className, { ast, vars }] of this.#classes.entries())
            if (espr.evaluate(ast, values))
                this.#el.classList.add(className);
    }

    async update(manager, oldState, newState, changed) {
        const values = this.getValues(newState);
        for (const [className, { ast, vars }] of this.#classes.entries())
            if (!changed.isDisjointFrom(vars))
                this.#el.classList[espr.evaluate(ast, values) ? "add" : "remove"](className);
        if (!this.#el.classList.length)
            this.#el.removeAttribute("class");
    }

    clear() {
        for (const className of this.#classes.keys())
            this.#el.classList.remove(className);
        if (!this.#el.classList.length)
            this.#el.removeAttribute("class");
    }
}

export default class ClassRenderer extends AttrRenderer {
    constructor() {
        super(ClassHandler);
    }
}

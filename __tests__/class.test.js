/* eslint no-return-assign: off */
/* eslint require-jsdoc: off */
/* eslint no-undef: off */
/* eslint no-console: off */

import { State, RenderingManager } from "@quotquot/element/dist/index.mjs";
import ClassRenderer from "../index.mjs";

test("Test class renderer", async() => {

    const state = new State({ x: 1, y: { z: 3 } });

    const renderer = new ClassRenderer();
    await renderer.setup(document.body, state);

    const manager = new RenderingManager();
    await manager.setup(document.body, state);
    manager.addRenderer(renderer);

    const template = document.createElement("template");
    template.innerHTML = '<div data-class="{foo: x === 2}">aaa</div>';
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe(template.innerHTML);

    await state.update({ x: 2 });
    expect(document.body.innerHTML).toBe('<div data-class="{foo: x === 2}" class="foo">aaa</div>');

    await state.update({ x: 1 });
    expect(document.body.innerHTML).toBe('<div data-class="{foo: x === 2}">aaa</div>');

    template.innerHTML = template.innerHTML.replaceAll("x === 2", "x === 1");
    manager.clear();
    await manager.appendTemplate(template, state.getAll());
    expect(document.body.innerHTML).toBe('<div data-class="{foo: x === 1}" class="foo">aaa</div>');

    manager.clear();
    await manager.cleanup(document.body, state);
    expect(document.body.innerHTML).toBe("");
});

import { ES6Parser, ES6StaticEval } from "espression";

function _variables(set, ...nodes) {
    for (const node of nodes) {
        switch (node.type.slice(0, 3)) {

            case "Lit": // Literal
            case "Thi": // ThisExpression
                break;

            case "Com": // Compound
                _variables(set, ...node.body);
                break;

            case "Arr": // ArrayExpression
                _variables(set, ...node.elements);
                break;

            case "Una": // UnaryExpression
                _variables(set, node.argument);
                break;

            case "Log": // LogicalExpression
            case "Bin": // BinaryExpression
                _variables(set, node.left, node.right);
                break;

            case "Con": // ConditionalExpression
                _variables(set, node.test, node.consequent, node.alternate);
                break;

            case "Cal": // CallExpression
                _variables(set, node.callee, ...node.arguments);
                break;

            case "Ide": // Identifier
                set.add(node.name);
                break;

            case "Mem": // MemberExpression
                switch (node.object.type.slice(0, 3)) {
                    case "Ide": // Identifier
                        set.add(node.object.name);
                        break;
                    case "Thi": // ThisExpression
                        set.add(node.property.name);
                        break;
                    case "Mem": // MemberExpression
                        _variables(set, node.object);
                        break;
                    default:
                        throw new Error(`Unhandled member expression ${node.object.type}`);
                }
                break;

            default:
                throw new Error(`unhandled node type ${node.type}`);
        }
    }
}

function variables(ast) {
    const set = new Set();
    _variables(set, ast);
    return set;
}


const parser = new ES6Parser();
const staticEval = new ES6StaticEval();

function getFromAttr(attr) {
    const ast = parser.parse(attr.value);
    if (ast.type !== "Program" ||
        ast.body.length !== 1 ||
        ast.body[0].type !== "ExpressionStatement")
        throw new Error("invalid expression");
    return ast.body[0].expression;
}


export default {
    getFromAttr,
    parse: parser.parse.bind(parser),
    evaluate: staticEval.evaluate.bind(staticEval),
    variables
};
